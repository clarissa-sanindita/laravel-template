<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Akun Baru!</h1>
<h4>Sign Up Form</h4>
    <form action="/nama" method ="POST">
    @csrf
        <label for="nama_depan">First name : </label> <br><br>
        <input type="text" name ="nama_depan"> <br><br>
        <label for="nama_belakang">Last name : </label> <br><br>
        <input type="text"> <br><br>

        <label for="">Gender :</label> <br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>

        <label for="">Nationality :</label><br><br>
        <select name="nasional" id="">
            <option value="1">Indonesia</option>
            <option value="2">Jepang</option>
            <option value="3">Belanda</option>
        </select><br><br>

        <label for="">Language Spoken :</label> <br><br>
        <input type="checkbox" id="nasional" name="nasional" value="indonesia">
        <label for="nasional">Bahasa Indonesia</label><br><br>
        <input type="checkbox" id="nasional" name="nasional" value="english">
        <label for="nasional">English</label><br><br>
        <input type="checkbox" id="nasional" name="nasional" value="other">
        <label for="nasional">Other</label><br><br>

        <label for="">Bio :</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>