<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome', function () {
    return view('welcomee');
});

Route::get('/register', 'AuthController@form');

Route::post('/nama', 'HomeController@sapa');

Route::get('/home', function () {
    return view('home');
});

Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/table', function () {
    return view('tugas.tabel');
});

Route::get('/data-tables', function () {
    return view('tugas.data-tabel');
});