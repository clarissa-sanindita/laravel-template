<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function sapa(Request $req){
        $nama = $req["nama_depan"];
        return view('welcomee', compact ('nama'));
    }
}
